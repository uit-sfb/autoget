#!/usr/bin/env python3
import os, pandas, collections, sys

# Start with ying (results in data/)
outputdir = sys.argv[1]
root_dir = os.path.join(outputdir, "data")
parsed = collections.defaultdict(str)
counter = 0

for root, dirs, files in os.walk(root_dir):
    # If data is missing
    if not files:
        continue
    # Must be in mmp-dir
    if not os.path.samefile(os.path.split(root)[0], root_dir):
        continue

    for filename in files:
        mmpid = os.path.split(root)[-1]
        counter += 1

        if not parsed[mmpid]:
            parsed[mmpid] = collections.defaultdict(str)

        if filename.startswith("checkm.txt"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.readlines()
                data = data[3].split()
                parsed[mmpid]['strain_heterogeneity'] = data[-1]
                parsed[mmpid]['contamination'] = data[-2]
                parsed[mmpid]['completeness'] = data[-3]


        if filename.startswith("trnas"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.readlines()
                if not data:
                    continue
                    #parsed[mmpid]['trnas_unique']=0
                    #parsed[mmpid]['trnas_total']=0
                else:
                    data = data[3:]
                    #print (data)
                    trnas = [x.split("\t")[4] for x in data]
                    parsed[mmpid]['trnas_unique']=len(set(trnas))
                    parsed[mmpid]['trnas_total']=len(trnas)

        if filename.startswith("seqkit"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.readlines()
                data = data[1].split()
                parsed[mmpid]['n50'] = data[12]
                parsed[mmpid]['num_seqs'] = data[3]
                parsed[mmpid]['sum_length'] = data[4]

        if filename.startswith("gc"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.read()
                parsed[mmpid]['gc'] = data.strip()

        if filename.startswith("5s"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.readlines()
                data = [x for x in data if not x.startswith("#")]
                parsed[mmpid]['5s'] = len(data)
                

        if filename.startswith("16s"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.readlines()
                data = [x for x in data if not x.startswith("#")]
                parsed[mmpid]['16s'] = len(data)

        if filename.startswith("23s"):
            with open(os.path.join(root,filename), "r") as result:
                data = result.readlines()
                data = [x for x in data if not x.startswith("#")]
                parsed[mmpid]['23s'] = len(data)

# Finish with yang (results in output/checkm and output/gtdb)
if os.path.isdir(os.path.join(outputdir, "checkm")):
    with open(os.path.join(outputdir,"checkm/results.txt"), "r") as result:
        data = result.readlines()
        data = data[3:]
        data = data[:-1]
        for line in data:
            line = line.split()
            mmpid = line[0].strip()
            parsed[mmpid]['checkm_strain_heterogeneity'] = line[-1].strip()
            parsed[mmpid]['checkm_contamination'] = line[-2].strip()
            parsed[mmpid]['checkm_completeness'] = line[-3].strip()

if os.path.isdir(os.path.join(outputdir, "gtdb")):
    if os.path.isfile(os.path.join(outputdir, "gtdb/gtdbtk.bac120.summary.tsv")):
        with open(os.path.join(outputdir,"gtdb/gtdbtk.bac120.summary.tsv"), "r") as result:
            data = result.readlines()
            data = data[1:]
            for line in data:
                line = line.split("\t")
                mmpid = line[0].strip()
                parsed[mmpid]['gtdb_classification'] = line[1].strip()
                parsed[mmpid]['gtdb_fastani_reference'] = line[2].strip()
                parsed[mmpid]['gtdb_fastani_ani'] = line[5].strip()

    if os.path.isfile(os.path.join(outputdir, "gtdb/gtdbtk.ar122.summary.tsv")):
        with open(os.path.join(outputdir,"gtdb/gtdbtk.bac120.summary.tsv"), "r") as result:
            data = result.readlines()
            data = data[1:]
            for line in data:
                line = line.split("\t")
                mmpid = line[0].strip()
                parsed[mmpid]['gtdb_classification'] = line[1].strip()
                parsed[mmpid]['gtdb_fastani_reference'] = line[2].strip()
                parsed[mmpid]['gtdb_fastani_ani'] = line[5].strip()

df = pandas.DataFrame(parsed)
#print(df.T.head)
df.T.to_csv(os.path.join(outputdir, "output.tsv"), sep="\t")

