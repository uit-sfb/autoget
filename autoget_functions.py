
def get_ncbi_gca_url(accession, attempt=1):
    '''
    This function creates the prefix string needed by wget (or curl) to download a specific Genbank assemvly (GCA) accession from NCBI
    Pass: GCA-accession (ex. GCA_000212675.2) and attempt number for timeout. If attempt reaches 3, returns False
    Returns: Ready built wget-prefix, (ex. https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/212/675/GCF_000212675.2_UCN72.1/GCF_001696305.1_UCN72.1_) or False. Append genomic.fna etc...
    '''
    if attempt==3:
        return False

    import requests, os, time
    from bs4 import BeautifulSoup

    prefix, digits = accession.split('_')
    digits, version = digits.split('.')
    sorted_triplets = [digits[i:i+3] for i in range(0, len(digits), 3)]

    url = "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/{}/{}/{}/".format(sorted_triplets[0], sorted_triplets[1], sorted_triplets[2])
    # We need to use bs4 and requests to find the right accession_name directory to append to our url as we do not have name information
    dir_lst = requests.get(url).text
    soup = BeautifulSoup(dir_lst, 'html.parser')
    directory = [node.get('href') for node in soup.find_all('a') if node.get('href').startswith(accession)]
    if not directory:
        print ("This url - accession - bs4-call is missing important info: {} - {}. Retrying in 3 secs...".format(url, accession, directory))
        time.sleep(3)
        attempt+=1
        return get_ncbi_gca_url(accession, attempt)
        # Implement some check here
    else:
        print ("This url - accession - bs4-call is ok: {} - {}".format(url, accession, directory))
        url = os.path.join(url + directory[0])
        url = os.path.join(url + directory[0])
        url = url[:-1]
        return url

def parse_mmp_metadata(filename, mmpcolumn, gcacolumn, noheader=False):
    terminate = False
    data = {}
    with open(filename, "r") as metadata:
        lines = metadata.readlines()
        # Skip header
        if not noheader == True:
            lines = lines[1:]
        lines = [x.split("\t") for x in lines]
        column_count = len(lines[0])
        for index, line in enumerate(lines, 1):
            line = [x.strip() for x in line]
            # If tsv format is distorted (not exactly the same number of columns as the header at any line, complain)
            if not len(line) == column_count:
                print ("Line {} in metadata file {} is malformed (contains {} columns, should be {})".format(index, filename, len(line), column_count))
                terminate = True
            data[line[mmpcolumn]] = line[gcacolumn]
        if terminate == True:
            exit("Terminated. Metadata is distorted (tsv)")

    return data

def download_files(reference, args, full=False):
    '''Downloads files from NCBI based on reference. '''
    import subprocess, os, multiprocessing
    from autoget_processing import proc_download, proc_unzip

    # Directory prefixes
    #print (os.getcwd(), outputdir)
    outputdir = os.path.join(os.getcwd(), 'output/downloaded')
    assemblydir = os.path.join(outputdir, "assembly")
    cdsdir = os.path.join(outputdir, "cds")
    protdir = os.path.join(outputdir, "prot")

    assembly = []
    cds = []
    prot = []

    print ("Creating download targets for {} files.".format(len(reference.keys())))
    for mmpid, accession in reference.items():
        url = get_ncbi_gca_url(accession)
        assembly.append((os.path.join(assemblydir, "{}.fa.gz".format(mmpid)), "{}_genomic.fna.gz".format(str(url))))
        cds.append((os.path.join(cdsdir, "{}.fna.gz".format(mmpid)), "{}_cds_from_genomic.fna.gz".format(str(url))))
        prot.append((os.path.join(protdir, "{}.faa.gz".format(mmpid)), "{}_protein.faa.gz".format(str(url))))

    # Make lists with tuples [(mmpid, url), (mmpid, url), (mmpid, url)] for proc_download()
    #assembly = [(os.path.join(assemblydir, "{}.fa.gz".format(mmpid)), "{}_genomic.fna.gz".format(str(get_ncbi_gca_url(accession)))) for mmpid,accession in reference.items()]
    #cds = [(os.path.join(cdsdir, "{}.fna.gz".format(mmpid)), "{}_cds_from_genomic.fna.gz".format(str(get_ncbi_gca_url(accession)))) for mmpid,accession in reference.items()]
    #prot = [(os.path.join(protdir, "{}.faa.gz".format(mmpid)), "{}_protein.faa.gz".format(str(get_ncbi_gca_url(accession)))) for mmpid,accession in reference.items()]
    
    print("Downloading and unziping {} files using wget ({} simultaneous threads)".format(len(reference.keys()), args.cpu))
    pool = multiprocessing.Pool(args.cpu)
    _ = pool.map(proc_download, assembly)
    _ = pool.map(proc_download, cds)
    _ = pool.map(proc_download, prot)
    _ = pool.map(proc_unzip, assembly)
    _ = pool.map(proc_unzip, cds)
    _ = pool.map(proc_unzip, prot)
    pool.close()
    pool.join()
    return (assembly, cds, prot)

def get_chunks2(data, size):
    return (data[pos:pos + size] for pos in range(0, len(data), size))

def get_chunks(data, SIZE=10000):
    from itertools import islice
    it = iter(data)
    for i in range(0, len(data), SIZE):
        yield {k:data[k] for k in islice(it, SIZE)}

def pre_stage(reference, outputdir='output'):
    import os
    if not os.path.isdir(outputdir):
        os.mkdir(outputdir)
    downloaddir = os.path.join(outputdir, "downloaded")
    if not os.path.isdir(downloaddir):
        os.mkdir(downloaddir)
    genomesdir = os.path.join(downloaddir, "assembly")
    if not os.path.isdir(genomesdir):
        os.mkdir(genomesdir)
    cdsdir = os.path.join(downloaddir, "cds")
    if not os.path.isdir(cdsdir):
        os.mkdir(cdsdir)
    protdir = os.path.join(downloaddir, "prot")
    if not os.path.isdir(protdir):
        os.mkdir(protdir)
    datadir = os.path.join(outputdir, "data")
    if not os.path.isdir(datadir):
        os.mkdir(datadir)
    for mmpid in reference:
        mmpdir = os.path.join(datadir, mmpid)
        if not os.path.isdir(mmpdir):
            os.mkdir(mmpdir)

def download_cleanup(processed, outputdir='output/downloaded'):
    import os, pickle
    missing_assemblies = []
    for root, dirs, files in os.walk(outputdir):
        for filename in files:
            if not os.stat(os.path.join(root, filename)).st_size > 0:
                #print ("Oops: {}".format(filename))
                os.unlink(os.path.join(root, filename))
                if root.endswith("assembly"):
                    #print ("Its an assembly file: {}".format(filename))
                    missing_assemblies.append(filename.split(".")[0])
    if missing_assemblies:
        print ("Warning - Missing {} assembly files. Removed from processed".format(len(missing_assemblies)))
        print (missing_assemblies)
        with open("output/missing_assemblies.p", "wb") as handle:
            pickle.dump(missing_assemblies, handle)

    processed['Downloaded'] = [acc for acc in processed['Downloaded'] if acc not in missing_assemblies]
    return processed

if __name__ == "__main__":
    import os
    outputdir = 'output'
    processed = {}
    processed['Downloaded'] = os.listdir(os.path.join(outputdir, 'downloaded/assembly'))
    processed['Downloaded'] = [acc.split(".")[0] for acc in processed['Downloaded']]
    processed = download_cleanup(processed, os.path.join(outputdir, "downloaded"))
    print (len(processed['Downloaded']))
    #print (processed)

