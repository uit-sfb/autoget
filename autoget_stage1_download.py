#!/usr/bin/env python3
import argparse, pickle, multiprocessing, os
from autoget_functions import *
from autoget_processing import *

parser = argparse.ArgumentParser(description='First stage of MMP_Autoget. This script downloads assembly, cds and protein files based on tsv metadata from Mar-databases (or any tsv file where needed accesions are given with --columns)')
parser.add_argument('--columns', help="Specify column numbers for mmpid and genbank accessions, respectively. Indexed by 0. (ex: [--columns 4:6] for mmpids in column 5 and genbank accessions in column 7. Default is 104:43)", type=str, default="104:43")
parser.add_argument('input', help="File with metadata in tsv format", type=str)
parser.add_argument('--noheader', action='store_true', help="(Mar databases) - Metadata tsv file has no header. Default: False")
parser.add_argument('--cpu', help="Simultaneous wget processes for the download stage. Default: 6", type=int, default=6)
parser.add_argument('--resume', action='store_true', help="Resume stopped processing / Add files from a different tsv file. output/ needs to be present")
args = parser.parse_args()

columns = args.columns.split(":")

# Parse mmp metadata. parse_mmp_metadata() returns dict with reference[mmpid] = GCA_accession
print ("Parsing metadata...")
reference = parse_mmp_metadata(args.input, int(columns[0]), int(columns[1]), args.noheader)

# Run pre_stage() to create folder structures if not resume. Uses all mmpids (from parse_mmp_metadata) to create separate mmp genomes folders
if os.path.isdir("output") and not args.resume:
    exit("output/ folder exists, but --resume is False. Use --resume to resume downloading.")

print ("Creating folder structures...")
pre_stage(reference.keys())

# If args.resume, load processed accessions from pickled object
if args.resume:
    with open("output/processed.p", "rb") as handle:
        processed = pickle.load(handle)
else:
    processed = {}
    processed['Downloaded'] = []

# Remove any processed mmpid from reference
reference = {mmpid:reference[mmpid] for mmpid, gca in reference.items() if mmpid not in processed['Downloaded']}

# Download files
filenames = download_files(reference, args)

# Clean any erroneous transactions from wget due to missing accessions, timeouts etc, etc,
processed = download_cleanup(processed)

# Save results
processed['Downloaded'] += reference.keys()
with open("output/processed.p", "wb") as handle:
    pickle.dump(processed, handle)

