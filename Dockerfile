# Incomplete
FROM continuumio/miniconda3
ENV HOME /root
RUN conda config --add channels conda-forge
RUN conda config --add channels bioconda
RUN conda create -n Mar_container python=3 requests=2.23.0 gtdbtk=1.0.2 biopython=1.76 beautifulsoup4=4.8.2 pandas=0.24.2 urllib3=1.25.7 trnascan-se=2.0 checkm-genome=1.1.2 seqkit=0.11.0 prokka=1.14.6
RUN conda create --name Autoget --file conda_dependencies.txt
RUN echo "source activate Autoget" > ~/.bashrc
ENV PATH /opt/conda/envs/Autoget/bin:$PATH
WORKDIR ${HOME}
