import multiprocessing, sys, os
import subprocess
from Bio import SeqIO, SeqUtils

'''
Dependencies needed:
    list
'''
def proc_download(pair):
    outputfile, url = pair[0], pair[1]
    cmd = "wget {} -N -t 3 -q -O {}".format(url, outputfile)
    return subprocess.call(cmd, shell=True)

def proc_unzip(pair):
    outputfile = pair[0]
    cmd = "gunzip -f -q {} 1>/dev/null 2>/dev/null".format(outputfile)
    return subprocess.call(cmd, shell=True)

def proc_gtdb(filename):
    from shutil import copyfile
    threads = 4
    # Make inputfolder and copy over genomic.fna, because gtdb is hard to work with
    mmpid = os.path.split(os.path.split(filename)[0])[-1]
    newinputfolder = "output/data/{}/gtdbinput".format(mmpid)
    newinputfolder = os.path.join(os.getcwd(), newinputfolder)
    newinputfile = os.path.join(newinputfolder, os.path.split(filename)[-1])
    os.mkdir(newinputfolder)
    copyfile(filename, newinputfile)
    cmd = "gtdbtk classify_wf --genome_dir {} --out_dir output/data/{}/gtdb --cpus {} 1>/dev/null 2>/dev/null".format(newinputfolder, mmpid, threads)
    return subprocess.call(cmd, shell=True)

def cp_gtdb_resultfile(filename):
    # If GTDB produces a bacteria or archaea output classification file, move it one directory up
    from shutil import copyfile
    mmpid = os.path.split(os.path.split(filename)[0])[-1]
    gtdbfolder = "output/data/{}/gtdb".format(mmpid)
    resultsfolder = "output/data/{}".format(mmpid)
    gtdbbacfile = os.path.join(gtdbfolder, 'gtdbtk.bac120.summary.tsv')
    gtdbarcfile = os.path.join(gtdbfolder, 'gtdbtk.ar122.summary.tsv')
    resultsbacfile = os.path.join(resultsfolder, 'gtdbtk.bac120.summary.tsv')
    resultsarcfile = os.path.join(resultsfolder, 'gtdbtk.ar122.summary.tsv')
    if os.path.isfile(gtdbbacfile):
        copyfile(gtdbbacfile, resultsbacfile)
    if os.path.isfile(gtdbarcfile):
        copyfile(gtdbarcfile, resultsarcfile)

def proc_prokka(filename):
    threads = 4
    mmpid = os.path.split(os.path.split(filename)[0])[-1]
    cmd = "prokka --force --cpu {} --outdir output/data/{}/prokka {} 1>/dev/null 2>/dev/null".format(threads, mmpid, filename)
    return subprocess.call(cmd, shell=True)

def proc_checkm(filename):
    from shutil import copyfile
    threads = 4
    # Make inputfolder and copy over genomic.fna, because checkm is hard to work with
    mmpid = os.path.split(os.path.split(filename)[0])[-1]
    newinputfolder = "output/data/{}/checkminput".format(mmpid)
    newinputfolder = os.path.join(os.getcwd(), newinputfolder)
    newtmpfolder = "output/data/{}/tmp".format(mmpid)
    newtmpfolder = os.path.join(os.getcwd(), newtmpfolder)
    newinputfile = os.path.join(newinputfolder, os.path.split(filename)[-1])
    os.mkdir(newinputfolder)
    os.mkdir(newtmpfolder)
    copyfile(filename, newinputfile)
    cmd = "checkm lineage_wf -f output/data/{}/checkm.txt -t {} -x fna --tmpdir {} {} output/data/{}/checkm 1>/dev/null 2>/dev/null".format(mmpid, threads, newtmpfolder, newinputfolder, mmpid)
    return subprocess.call(cmd, shell=True)

def cleanup_checkm():
    pass

def proc_trnas(filename):
    mmpid = filename[0]
    filename = filename[1]
    cmd = "tRNAscan-SE -q -B -o output/data/{}/trnas.txt {}".format(mmpid, filename)
    return subprocess.call(cmd, shell=True)

def proc_rrna5s(filename):
    model = "5S_rfam.cm"
    mmpid = filename[0]
    filename = filename[1]
    cmd = "cmsearch -Z 1000 --cut_ga --tblout output/data/{}/5s.txt {} {} 1>/dev/null 2>&1".format(mmpid, model, filename)
    return subprocess.call(cmd, shell=True)

def proc_rrna16s(filename):
    model = "16S_rfam.cm"
    mmpid = filename[0]
    filename = filename[1]
    cmd = "cmsearch -Z 1000 --cut_ga --tblout output/data/{}/16s.txt {} {} 1>/dev/null 2>&1".format(mmpid, model, filename)
    return subprocess.call(cmd, shell=True)

def proc_rrna23s(filename):
    model = "23S_rfam.cm"
    mmpid = filename[0]
    filename = filename[1]
    cmd = "cmsearch -Z 1000 --cut_ga --tblout output/data/{}/23s.txt {} {} 1>/dev/null 2>&1".format(mmpid, model, filename)
    return subprocess.call(cmd, shell=True)

def proc_gc(filename):
    mmpid = filename[0]
    filename = filename[1]
    totalseq = ''
    for record in SeqIO.parse(filename, "fasta"):
        totalseq = totalseq + record.seq
    GC = SeqUtils.GC(totalseq)
    cmd = "echo {} > output/data/{}/gc.txt".format(GC, mmpid)
    return subprocess.call(cmd, shell=True)

def proc_stats(filename):
    mmpid = filename[0]
    filename = filename[1]
    cmd = "seqkit stats -a {} > output/data/{}/seqkit.txt".format(filename, mmpid)
    return subprocess.call(cmd, shell=True)


if __name__ == '__main__':

    #argparse
    proc_checkm(sys.argv[1])
    exit()

    rRNA_models = ['5S_rfam.cm','16S_rfam.cm','23S_rfam.cm']
    root_dir = sys.argv[1]
    filenames = []
    count = 0
    for root, dirs, files in os.walk(root_dir):
        for filename in files:
            if filename.endswith(".fna"):
                count +=1
                #if count == 5:
                #    break
                filename = os.path.join(root,filename)
                filenames.append(filename)


    pool = multiprocessing.Pool(60)
    #pess = []
    #trnas = pool.map(trnas, filenames)
    #rrna5s = pool.map(rrna5s, filenames)
    #rrna16s = pool.map(rrna16s, filenames)
    #rrna23s = pool.map(rrna23s, filenames)
    #gc = pool.map(gc, filenames)
    #the_rest = pool.map(the_rest, filenames)
    gtdb = pool.map(proc_gtdb, staged_files)
    gtdb = pool.map(cp_gtdb_resultfile, staged_files)
    pool.close()
    pool.join()
