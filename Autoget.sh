#!/bin/sh
# This script wraps all stages of Autoget neatly into a single command
# Run Autoget.sh <metadata file> to perform all steps required for a complete Mar database update
# <metadata file> needs to contain only novel entries
./autoget_stage1_download.py $1
./autoget_stage2_process_yang.py --cpu 50
./autoget_stage2_process_ying.py --cpu 50
./autoget_stage3_parse_results.py output
