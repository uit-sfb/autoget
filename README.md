# Autoget - Mar databases pre-processing pipeline

Autoget pre-processes all entries staged for inclusion in any Mar database division to ensure a uniform and comparable set of metadata.

## Overview

Autoget is divided into 4 discrete modules in 3 stages:
- **Download** - The download module parses a given tsv file with IDs (ex. MMPID) and NCBI GCA (assembly) accessions present in two distinct columns, typically Current.tsv for the latest release of any Mar division (MarRef, MarDB, SalDB etc.). Python Requests and Beautifulsoup are used to browse and accession-check each entry, as well as downloading all available data for each entry. 

- **Ying (tools)** / **Yang (pipelines)** - Processing with Autoget is divided into two separate workflows for optimization reasons. Bioinformatic tools are notorious for being extremely diverse in terms of software implementation, which often makes it challenging to combine them into workflows that run efficiently. (As an example, running CheckM on 1 genome 1000 times forked to 50 threads with f.ex python multiprocessing takes over 24 hours due to overhead++, while running it as intended with one parent process and --cpu 50 takes under one hour)
As a result the Ying script manages all software that is optimized for running single instances x CPU times concurrently, mostly standalone tools such as tRNAScan, SeqKit etc. The yang script manages tools that are inherent pipelines in them selves and runs most efficiently while managing resource parameters on their own, such as CheckM and GTDB. 

- **Parser** - The parser iterates through the output/ folder looking for specific result files, parses them and prints the final output.tsv file for manual merging with Mar databases metadata

```mermaid 
graph LR; NCBI--GCA-->Download; output/-->Ying; Download-->output/; output/-->Yang; output/-->Parser; Yang--Pipelines-->output/pipeline; Ying--Tools-->output/data/ID/tool; Parser-->output/output.tsv; output/pipeline-->Parser; output/data/ID/tool-->Parser
```


## Usage
**Installation**

All dependencies needed for this software to work is listed in [conda_dependencies.txt](conda_dependencies.txt)
These can be installed by running the conda command: **conda create --name \<env> --file conda_dependencies.txt**

**Main wrapper**

[Autoget.sh](Autoget.sh) is a convenience wrapper that constitutes a complete Autoget processing run (all stages from start to finish) This wrapper is optimized for SfB infrastructure and makes parameter assumptions for all stages
Usage ex: `./Autoget.sh Current.tsv`

**Stages**
Each stage can be run separately with parameters for cpu usage, parsing, cleanup++. Use -h for help.

`./autoget_stage1_download.py -h`

`./autoget_stage2_ying.py -h`

`./autoget_stage2_yang.py -h`

`./autoget_stage3_parse_results.py -h`

## Software overview
| Tool/pipelines   |      Version      |  DB-version |
|----------|:-------------:|------:|
| CheckM |  1.1.2 | (default, 2015)|
| GTDBTK |    1.0.2   | release 89|
| tRNAScan-SE | 2.0.5 | |
| cmsearch (rRNA) | 1.1.2 (Infernal) | Rfam 14.1|
| Biopython (GC) | 1.76 |  |
| SeqKit | 0.11.0 |  |

## Performance (pr. 1000 genomes)
Download - **~20 minutes** (depending on connetion)

Ying - 50 threads - **~1 hour**

Yang - 50 threads - 6 pplacer threads - **~1 hour**

Parser - **negligible**

**Expect 2,5 hours of runtime pr. 1000 genomes with these parameters**

## TODO
- Make parameter for pplacer threads (yang) for more dynamic memory management.
- Write checks for what additional files are downloaded in stage 1
 file
 - ... 
